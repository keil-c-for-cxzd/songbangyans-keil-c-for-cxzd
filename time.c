#include"config.h"

/*************定时器初始化*****************/
void time_init(void)
	{
	TMOD = 0x11;
	TH0 = 0x70;
	TL0 = 0x00;
	TH1 = 0x70;
	TL1 = 0x00;
	TR0 = 1;
	ET0 = 1;
	TR1 = 1;
	ET1 = 1;
	EA = 1;
	Now_time.msec = 0;
	Counter = 0;
	Time_Counter = 0;
	Lcd_counter = 0;
	Data_time = 0;
	Key_value = 0;
	Key_count = 0;
	}



/*************定时器0中断处理******************/
time_interrupt(void) interrupt 1 using 1
	{
	Now_time.msec++;
	if(Now_time.msec>=50)
		{
		Now_time.msec = 0;
		Now_time.sec++;
		Lcd_counter++;
		Data_time++;
		Alarm_count++;
		Time_Counter++;	
		if(Now_time.sec>=60)
			{
			Now_time.sec = 0;
			Now_time.min++;			
			if(Now_time.min>=60)
				{
				Now_time.min = 0;
				Now_time.hour++;  				
				if(Now_time.hour>=24)
					{
					Now_time.hour = 0;
					}
				}
			}
		}
	if(Time_Counter >= TIMEING)				//超过TIMEING未校时，认为时间已经失效
		{
		timeout = 1;
		}
	if(Lcd_counter>=LCD_ONTIME)
		{
		lcd_flag = 0;
		feed_dog = 1;
		Lcd_counter = 0;
		Key0_value = 0;
		Key_value = 0;
		linktest = 0;
		}
	if(Data_time>=DATA_TIMR)
		{
		data_start = 0;
		buffer_flag = 0;
		point = 0;
		}
	if((Alarm_count > ALARM_TURE)&&(alarm_time0))
		{
		alarm_time0 = 0;
		Alarm_count	= 0;
		}
	if((Alarm_count > ALARM_FAUSE)&&(!alarm_time0))
		{
		alarm_time0 = 1;
		Alarm_count	= 0;
		}
	TH0 = 0x70;
	TL0 = 0x00;
	}


/*************定时器1中断处理******************/
time1_interrupt(void) interrupt 3 using 2
	{
	Counter++;
	if(Counter >= 100)
		{
		Counter = 0;
		}
	if(alarm_time)											//警报处理
		{
		if(((Counter%10)==0)&&(alarm_flg!=0))
			{
			if((alarm_canc)||(!alarm_time0))
				{
				ALARM = 1;
				A_LED = ~A_LED;
				}
			else
				{
				ALARM = ~ALARM;
				A_LED = ~A_LED;
				}
			}
		}
	if((Counter%25) == 0)
		{
		lcd_refur = 1;
		}
	if((KEYUP == 0)&&(key_up == 1))										    //向上键处理
		{
		Key_count++;
		Lcd_counter = 0;
		}  
	if(Key_count >= KEYDELAY)
		{
		if(lcd_flag == 1)
			{			
			Key_count = 0;
			Key_value++;
			key_plus = 1;
			Key0_value = 0;
			key_up = 0;
			}
		else
			{
			lcd_flag = 1;
			Key_count = 0;
			key_plus = 1;
			Key0_value = 0;
			key_up = 0;
			}
		}
	if((KEYDOWN == 0)&&(key_up == 1))										 //向下键处理
		{
		Key1_count++;
		Lcd_counter = 0;
		}
	if(Key1_count >= KEYDELAY)
		{
		if(lcd_flag == 1)
			{
			Key1_count = 0;
			Key_value--;
			key_plus = 0;
			Key0_value = 0;
			key_up = 0;			
			}
		else
			{
			lcd_flag = 1;
			Key1_count = 0;
			key_plus = 0;
			Key0_value = 0;
			key_up = 0;
			}
		} 
	if((KEYCANC == 0)&&(key_up == 1))										   //确认键处理
		{
		lcd_flag = 1;
		Key2_count++;
		Key0_count++; 		
		}
	if(Key2_count == CANCALALARMTIME)					   
		{
		Key0_value = 0xff;	
		}
	if(Key0_count >= KEYLONG)
		{
		Key0_value = 0x55;
		Key0_count = 0;
		key_up = 0;
		}
	if((KEYUP == 1)&&(KEYDOWN == 1)&&(KEYCANC == 1)) 					//按键释放处理
		{
		Key_upcount++;
		}
	if(Key_upcount >= 0x03)
		{
		key_up = 1;
		Key_count = 0;
		Key1_count = 0;
		Key2_count = 0;
		Key0_count = 0;
		Key_upcount = 0; 
		}
	TH1 = 0x70;
	TL1 = 0x00;
	}

