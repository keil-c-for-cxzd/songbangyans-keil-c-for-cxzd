#include"config.h"
EXT_ uchar code ASCII[];
EXT_ uchar code ZM[];

/***************按键处理函数*******************/
void Key_fun(void)
	{
	LCD_open();
	if(key_plus)
		{
		if((Key_value == KYUE)&&(!set_leaje))					 //剩余金额
			{
			Key_value = KYDL;
			}
		if((Key_value == KYDL)&&(!set_leadl))					//剩余电量
			{
			Key_value = KZQS;
			}
		if((Key_value >= KZQS)&&(Key_value <= KZQE)&&(!set_alljs))	     //周期电量
			{
			Key_value = KDQS;
			}
		if((Key_value >= KDQS)&&(Key_value <= KDQE)&&(!set_nowdl))		 //当前示数
			{
			Key_value = KDJS;
			}
		if((Key_value >= KDJS)&&(Key_value <= KDJE)&&(!set_dj))			//电价
			{
			Key_value = KGDC;
			}
		if((Key_value == KGDC)&&(!set_timegd))						 	//总购电次数
			{
			Key_value = KLGD;
			}
		if((Key_value == KLGD)&&(!set_alldl))						 	//累计购电量
			{
			Key_value = KLJJ;
			}
		if((Key_value == KLJJ)&&(!set_ljgdje))						 	//累计购电金额
			{
			Key_value = KGDT;
			}
		if((Key_value == KGDT)&&(!set_lastgd))						 	//最后一次购电时间
			{
			Key_value = KGDJ;
			}
		if((Key_value == KGDJ)&&(!set_lgdt))						 	//最后一次购电金额
			{
			Key_value = KDQJ;
			}
		if((Key_value == KDQJ)&&(!set_dqjtdj))						 	//当前阶梯电价
			{
			Key_value = KGXT;
			}
		if((Key_value == KGXT)&&(!timing_flag))						 	//最后更新时间
			{
			Key_value = 0;
			}
		if(Key_value >= KGXT+1)
			{
			Key_value =	0;
			}
		}
	else
		{
		if(Key_value >= KGXT)
			{
			Key_value =	KGXT;
			}
		if((Key_value == KGXT)&&(!timing_flag))					 		 //最后更新时间
			{
			Key_value = KDQJ;
			}
		if((Key_value == KDQJ)&&(!set_dqjtdj))							//当前阶梯电价
			{
			Key_value = KGDJ;
			}
		if((Key_value == KGDJ)&&(!set_lgdt))						 	//最后一次购电金额
			{
			Key_value = KGDT;
			}
		if((Key_value == KGDT)&&(!set_lastgd))						 	//最后一次购电时间
			{													   
			Key_value = KLJJ;
			}							
		if((Key_value == KLJJ)&&(!set_ljgdje))						 	//累计购电金额
			{
			Key_value = KLGD;
			}
		if((Key_value == KLGD)&&(!set_alldl))						 	//累计购电量
			{
			Key_value = KGDC;
			}	
		if((Key_value == KGDC)&&(!set_timegd))						 	//总购电次数
			{
			Key_value = KDJE;
			}
		if((Key_value >= KDJS)&&(Key_value <= KDJE)&&(!set_dj))			//电价
			{
			Key_value = KDQE;
			}
		if((Key_value >= KDQS)&&(Key_value <= KDQE)&&(!set_nowdl))		 //当前示数
			{
			Key_value = KZQE;
			}
		if((Key_value >= KZQS)&&(Key_value <= KZQE)&&(!set_alljs))	  	 //周期电量
			{
			Key_value = KYDL;
			}
		if((Key_value == KYDL)&&(!set_leadl))							//剩余电量
			{
			Key_value = KYUE;
			}
		if((Key_value == KYUE)&&(!set_leaje))							 //剩余金额
			{
			Key_value = 0;
			}
		} 		
	switch(Key_value)
		{
		case(KYUE):
			Dis_LeaJ();
			break;
		case(KYDL):
			Dis_LeaD();
			break;
		case(KZQS):
		case(KZQS+1):
		case(KZQS+2):
		case(KZQS+3):
		case(KZQE):
			Dis_Dqz();
			break;
		case(KDQS):
		case(KDQS+1):
		case(KDQS+2):
		case(KDQS+3):
		case(KDQE):
			Dis_Dqdls();
			break;
		case(KDJS):
		case(KDJS+1):
		case(KDJS+2):
		case(KDJE):	
			Dis_DJ();
			break;
		case(KGDC):
			Dis_GCount();
			break;
		case(KLGD):
			Dis_GTotal();
			break;
		case(KLJJ):
			Dis_Tbuyj();
			break;
		case(KGDT):
			Dis_Lastbuytime();
			break;
		case(KGDJ):
			Dis_Lastbuyj();
			break;
		case(KDQJ):
			Dis_Nowjtdj();
			break;
		case(KGXT):
			Dis_Lasttiming();
			break;
		default:
			Dis_dbadd();
			break;
			
		}
	}

/***************显示本机地址*******************/
void Dis_localadd(void)
	{
	uchar temp[12];
	uchar i; 
	for(i=0;i<6;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_MAC+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,0);	   	//本
	drawword(16,0,1);		//机
	drawword(32,0,38);		//地
	drawword(48,0,39);		//址
	drawASCII(64,0,63);		//：
	drawword(72,0,40);		//空格
	drawword(80,0,40);
	drawword(96,0,40);
	drawword(112,0,40);
	drawword(0,2,40);
	drawword(10,2,40);
	drawASCII(26,2,temp[11]);
	drawASCII(34,2,temp[10]);
	drawASCII(42,2,temp[9]);
	drawASCII(50,2,temp[8]);
	drawASCII(58,2,temp[7]);
	drawASCII(66,2,temp[6]);
	drawASCII(74,2,temp[5]);
	drawASCII(82,2,temp[4]);
	drawASCII(90,2,temp[3]);
	drawASCII(98,2,temp[2]);
	drawASCII(106,2,temp[1]);
	drawASCII(114,2,temp[0]);
	}

/***************显示电表地址*******************/
void Dis_dbadd(void)
	{
	uchar temp[12];
	uchar i;
	for(i=0;i<6;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_ADD+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	if(0x0a <= temp[0])
		{
		Dis_localadd();
		}
	else
		{
		drawword(0,0,12);	   	//电
		drawword(16,0,37);	  	//表
		drawword(32,0,37);		//表
		drawword(48,0,52);		//号
		drawASCII(64,0,63);		//：
		drawword(72,0,40);		//空格
		drawword(88,0,40);
		drawword(104,0,40);
		drawword(120,0,40);
		drawword(0,2,40);
		drawword(10,2,40);
		drawASCII(26,2,temp[11]);
		drawASCII(34,2,temp[10]);
		drawASCII(42,2,temp[9]);
		drawASCII(50,2,temp[8]);
		drawASCII(58,2,temp[7]);
		drawASCII(66,2,temp[6]);
		drawASCII(74,2,temp[5]);
		drawASCII(82,2,temp[4]);
		drawASCII(90,2,temp[3]);
		drawASCII(98,2,temp[2]);
		drawASCII(106,2,temp[1]);
		drawASCII(114,2,temp[0]);
		}
	if((Key_value>=(KGXT+1))&&(key_plus))
		{
		Key_value = 0;
		}
	if((Key_value>=(KGXT+1))&&(!key_plus))
		{
		Key_value = KGXT;
		}
	}

/***************显示费率电价*******************/
void Dis_DJ(void)
	{
	uchar temp[8];
	uchar i;
	drawASCII(0,0,10);		//" "
	if(Key_value == KDJS)
		{
		drawword(8,0,6);	   	//尖
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ELEVALUE+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDJS+1))
		{
		drawword(8,0,7);	   	//峰
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ELEVALUE+4+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDJS+2))
		{
		drawword(8,0,8);	   	//平
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ELEVALUE+8+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDJS+3))
		{
		drawword(8,0,9);	   	//谷
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ELEVALUE+12+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	drawASCII(24,0,10);	  	//" "
	drawword(32,0,10);		//费
	drawword(48,0,11);		//率
	drawword(64,0,12);		//电
	drawword(80,0,13);		//价	
	drawASCII(96,0,63);		//：
	drawword(104,0,40);	   	//空格
	drawword(120,0,40);	  
	for(i=7;i>=5;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(2,2,temp[7]);
	drawASCII(10,2,temp[6]);
	drawASCII(18,2,temp[5]);
	drawASCII(26,2,temp[4]);
	drawASCII(34,2,66);		  //"."
	drawASCII(42,2,temp[3]);
	drawASCII(50,2,temp[2]);
	drawASCII(58,2,temp[1]);
	drawASCII(66,2,temp[0]);
	drawword(74,2,41);		 //元
	drawASCII(90,2,67);		 //"/"	
	drawASCII(98,2,21);		 //k
	drawASCII(106,2,59);	 //W
	drawASCII(114,2,18);	 //h
	}



/***************当前电量示数*******************/
void Dis_Dqdls(void)
	{
	uchar temp[8];
	uchar i;

	drawword(0,0,14);		//当
	drawword(16,0,15);	  	//前
	if(Key_value == KDQS)
		{
		drawword(32,0,42);		//总
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_NELEVAL+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDQS+1))
		{
		drawword(32,0,6);		//尖
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_NELEVAL+4+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDQS+2))
		{
		drawword(32,0,7);		//峰
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_NELEVAL+8+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDQS+3))
		{
		drawword(32,0,8);		//平
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_NELEVAL+12+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KDQS+4))
		{
		drawword(32,0,9);		//谷
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_NELEVAL+16+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	drawword(48,0,12);		//电
	drawword(64,0,24);		//量
	drawword(80,0,20);		//示
	drawword(96,0,21);		//数	
	drawASCII(112,0,63);	//：
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(10,2,40);	   	//空格
	for(i=7;i>=3;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(26,2,temp[7]);
	drawASCII(34,2,temp[6]);
	drawASCII(42,2,temp[5]);
	drawASCII(50,2,temp[4]);
	drawASCII(58,2,temp[3]);		  
	drawASCII(66,2,temp[2]);
	drawASCII(74,2,66);		  //"."
	drawASCII(82,2,temp[1]);
	drawASCII(90,2,temp[0]);
	drawASCII(98,2,21);		 //k
	drawASCII(106,2,59);	 //W
	drawASCII(114,2,18);	 //h
	}


	
/***************本周期用电量*******************/
void Dis_Dqz(void)
	{
	uchar temp[8];
	uchar i;

	drawword(0,0,0);		//本
	drawword(16,0,34);	  	//周
	drawword(32,0,35);		//期
	if(Key_value == KZQS)
		{
		drawword(48,0,42);		//总
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ALLDL+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KZQS+1))
		{
		drawword(48,0,6);		//尖
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ALLDL+4+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KZQS+2))
		{
		drawword(48,0,7);		//峰
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ALLDL+8+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KZQS+3))
		{
		drawword(48,0,8);		//平
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ALLDL+12+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}
	else if(Key_value == (KZQS+4))
		{
		drawword(48,0,9);		//谷
		for(i=0;i<4;i++)
			{
			temp[2*i]=Byte_Read(LOCAL_ALLDL+16+i);
			temp[2*i+1]=(temp[2*i])>>4;
			temp[2*i]=temp[2*i]&0x0f;
			}
		}

	drawword(64,0,36);		//用
	drawword(80,0,12);		//电
	drawword(96,0,24);		//量	
	drawASCII(112,0,63);	//：
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(10,2,40);	   	//空格
	for(i=7;i>=3;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(26,2,temp[7]);
	drawASCII(34,2,temp[6]);
	drawASCII(42,2,temp[5]);
	drawASCII(50,2,temp[4]);
	drawASCII(58,2,temp[3]);		  
	drawASCII(66,2,temp[2]);
	drawASCII(74,2,66);		  //"."
	drawASCII(82,2,temp[1]);
	drawASCII(90,2,temp[0]);
	drawASCII(98,2,21);		 //k
	drawASCII(106,2,59);	 //W
	drawASCII(114,2,18);	 //h
	}


/***************当前剩余电量*******************/
void Dis_LeaD(void)
	{
	uchar temp[10];
	uchar i;
	for(i=0;i<5;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_NELELEAD+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,14);		//当
	drawword(16,0,15);	  	//前
	if(temp[0] == 0)
		{
		drawword(32,0,22);		//剩
		drawword(48,0,23);		//余		
		}
	else
		{
		drawword(32,0,2);		//透
		drawword(48,0,3);		//支		
		}
	drawword(64,0,12);		//电
	drawword(80,0,24);		//量
	drawASCII(96,0,63);		//：	
	drawword(104,0,40);	
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(10,2,40);	   	//空格
	for(i=9;i>=5;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(26,2,temp[9]);
	drawASCII(34,2,temp[8]);
	drawASCII(42,2,temp[7]);
	drawASCII(50,2,temp[6]);
	drawASCII(58,2,temp[5]);		  
	drawASCII(66,2,temp[4]);
	drawASCII(74,2,66);		  //"."
	drawASCII(82,2,temp[3]);
	drawASCII(90,2,temp[2]);
	drawASCII(98,2,21);		 //k
	drawASCII(106,2,59);	 //W
	drawASCII(114,2,18);	 //h
	}


/***************当前剩余金额*******************/
void Dis_LeaJ(void)
	{
	uchar temp[10];
	uchar i;
	for(i=0;i<5;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_NELELEAJ+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,14);		//当
	drawword(16,0,15);	  	//前
	if(temp[0] == 0)
		{
		drawword(32,0,22);		//剩
		drawword(48,0,23);		//余		
		}
	else
		{
		drawword(32,0,2);		//透
		drawword(48,0,3);		//支		
		}
	drawword(64,0,25);		//金
	drawword(80,0,26);		//额
	drawASCII(96,0,63);		//：
	drawword(104,0,40);		//空格
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(12,2,40);	   	//空格
	drawword(18,2,40);	   	//空格
	for(i=9;i>=5;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(34,2,temp[9]);
	drawASCII(42,2,temp[8]);
	drawASCII(50,2,temp[7]);
	drawASCII(58,2,temp[6]);
	drawASCII(66,2,temp[5]);		  
	drawASCII(74,2,temp[4]);
	drawASCII(82,2,66);		  //"."
	drawASCII(90,2,temp[3]);
	drawASCII(98,2,temp[2]);
	drawword(106,2,41);		 //元
	}

/***************总购电次数*******************/
void Dis_GCount(void)
	{
	uchar temp[4];
	uchar i;
	for(i=0;i<2;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_BUYCONUT+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	if(temp[3] == 0x0f)
		{
		_nop_();
		}
	else
		{
		drawword(0,0,42);		//总
		drawword(16,0,28);	  	//购
		drawword(32,0,12);		//电
		drawword(48,0,29);		//次
		drawword(64,0,21);		//数
		drawASCII(80,0,63);		//：
		drawword(88,0,40);		//空格	
		drawword(104,0,40);		//空格
		drawword(120,0,40);	   	//空格
		drawword(0,2,40);	   	//空格
		drawword(16,2,40);	   	//空格
		drawword(32,2,40);	   	//空格
		drawword(42,2,40);	   	//空格
		drawword(58,2,40);	   	//空格
		for(i=3;i>=1;i--)
			{
			if(temp[i] == 0)
				{
				temp[i] = 10;
				}
			else
				{
				break;
				}
			}
		drawASCII(74,2,temp[3]);		  
		drawASCII(82,2,temp[2]);
		drawASCII(90,2,temp[1]);
		drawASCII(98,2,temp[0]);
		drawword(106,2,29);		 //次
		}
	}


/***************最后一次购电时间*******************/
void Dis_Lastbuytime(void)
	{
	uchar temp[10];
	uchar i;
	for(i=0;i<5;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_LASTBUYT+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}

	drawword(0,0,53);				//最
	drawword(16,0,54);				//近
	drawword(32,0,28);				//购
	drawword(48,0,12);				//电
	drawword(64,0,45);				//时
	drawword(80,0,49);				//间
	drawASCII(96,0,63);				//:
	drawword(104,0,40);
	drawword(112,0,40);
	drawword(0,2,40);
	drawword(10,2,40);						
	drawASCII(26,2,temp[7]);
	drawASCII(34,2,temp[6]);
	drawword(42,2,51);						 //月
	drawASCII(58,2,temp[5]);
	drawASCII(66,2,temp[4]);
	drawword(74,2,50);						 //日
	drawASCII(90,2,temp[3]);				 
	drawASCII(98,2,temp[2]);  
	drawword(106,2,45);						 //时
	}


/***************累计购电量*******************/
void Dis_GTotal(void)
	{
	uchar temp[8];
	uchar i;
	for(i=0;i<4;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_BUYTAT+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	if(temp[3] == 0x0f)
		{
		_nop_();
		}
	else
		{
		drawword(0,0,30);		//累
		drawword(16,0,31);	  	//计
		drawword(32,0,28);		//购
		drawword(48,0,12);		//电
		drawword(64,0,24);		//量
		drawASCII(80,0,63);		//：
		drawword(88,0,40);		//空格	
		drawword(104,0,40);		//空格
		drawword(120,0,40);	   	//空格
		drawword(0,2,40);	   	//空格
		drawword(10,2,40);	   	//空格
		for(i=7;i>=3;i--)
			{
			if(temp[i] == 0)
				{
				temp[i] = 10;
				}
			else
				{
				break;
				}
			}
		drawASCII(26,2,temp[7]);
		drawASCII(34,2,temp[6]);
		drawASCII(42,2,temp[5]);
		drawASCII(50,2,temp[4]);
		drawASCII(58,2,temp[3]);		  
		drawASCII(66,2,temp[2]);
		drawASCII(74,2,66);		  //"."
		drawASCII(82,2,temp[1]);
		drawASCII(90,2,temp[0]);
		drawASCII(98,2,21);		 //k
		drawASCII(106,2,59);	 //W
		drawASCII(114,2,18);	 //h
		}
	}



/****************累计购电金额***********************/
void Dis_Tbuyj(void)
	{
	uchar temp[8];
	uchar i;
	for(i=0;i<4;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_LJGDJNE+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,30);		//累
	drawword(16,0,31);	  	//计
	drawword(32,0,28);		//购
	drawword(48,0,12);		//电
	drawword(64,0,25);		//金
	drawword(80,0,26);		//额
	drawASCII(96,0,63);		//：
	drawword(104,0,40);		//空格	
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(10,2,40);	   	//空格
	for(i=7;i>=3;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(26,2,10);
	drawASCII(34,2,temp[7]);
	drawASCII(42,2,temp[6]);
	drawASCII(50,2,temp[5]);
	drawASCII(58,2,temp[4]);		  
	drawASCII(66,2,temp[3]);
	drawASCII(74,2,temp[2]);		  
	drawASCII(82,2,66);		  //"."
	drawASCII(90,2,temp[1]);
	drawASCII(98,2,temp[0]);		 
	drawword(106,2,41);		  //元
	}


/****************最后一次购电金额***********************/
void Dis_Lastbuyj(void)
	{
	uchar temp[8];
	uchar i;
	for(i=0;i<4;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_LASTGDJE+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,53);		//最
	drawword(16,0,54);	  	//近
	drawword(32,0,28);		//购
	drawword(48,0,12);		//电
	drawword(64,0,25);		//金
	drawword(80,0,26);		//额
	drawASCII(96,0,63);		//：
	drawword(104,0,40);		//空格	
	drawword(120,0,40);	   	//空格
	drawword(0,2,40);	   	//空格
	drawword(10,2,40);	   	//空格
	for(i=7;i>=3;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(26,2,10);
	drawASCII(34,2,temp[7]);
	drawASCII(42,2,temp[6]);
	drawASCII(50,2,temp[5]);
	drawASCII(58,2,temp[4]);		  
	drawASCII(66,2,temp[3]);
	drawASCII(74,2,temp[2]);		  
	drawASCII(82,2,66);		  //"."
	drawASCII(90,2,temp[1]);
	drawASCII(98,2,temp[0]);		 
	drawword(106,2,41);		  //元
	}
/****************当前阶梯电价***********************/
void Dis_Nowjtdj(void)
	{
	uchar temp[8];
	uchar i;
	for(i=0;i<4;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_LASTGDJE+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,14);		//当
	drawword(16,0,15);		//前
	drawword(32,0,4);		//阶
	drawword(48,0,5);		//梯
	drawword(64,0,12);		//电
	drawword(80,0,13);		//价	
	drawASCII(96,0,63);		//：
	drawword(104,0,40);	   	//空格
	drawword(120,0,40);	  
	for(i=7;i>=5;i--)
		{
		if(temp[i] == 0)
			{
			temp[i] = 10;
			}
		else
			{
			break;
			}
		}
	drawASCII(2,2,temp[7]);
	drawASCII(10,2,temp[6]);
	drawASCII(18,2,temp[5]);
	drawASCII(26,2,temp[4]);
	drawASCII(34,2,66);		  //"."
	drawASCII(42,2,temp[3]);
	drawASCII(50,2,temp[2]);
	drawASCII(58,2,temp[1]);
	drawASCII(66,2,temp[0]);
	drawword(74,2,41);		 //元
	drawASCII(90,2,67);		 //"/"	
	drawASCII(98,2,21);		 //k
	drawASCII(106,2,59);	 //W
	drawASCII(114,2,18);	 //h
	}
/****************上一次校时时间***********************/
void Dis_Lasttiming(void)
	{
	uchar temp[12];
	uchar i;
	for(i=0;i<6;i++)
		{
		temp[2*i]=Byte_Read(LOCAL_TIMING+i);
		temp[2*i+1]=(temp[2*i])>>4;
		temp[2*i]=temp[2*i]&0x0f;
		}
	drawword(0,0,21);				//数
	drawword(16,0,46);				//据
	drawword(32,0,47);				//更
	drawword(48,0,48);				//新
	drawword(64,0,45);				//时
	drawword(80,0,49);				//间
	drawASCII(96,0,63);				//:
	drawword(104,0,40);
	drawword(112,0,40);
	drawword(0,2,40);
	drawword(10,2,40);						
	drawASCII(26,2,temp[9]);
	drawASCII(34,2,temp[8]);
	drawword(42,2,51);						 //月
	drawASCII(58,2,temp[7]);
	drawASCII(66,2,temp[6]);
	drawword(74,2,50);						 //日
	drawASCII(90,2,temp[5]);				 
	drawASCII(98,2,temp[4]);  
	drawword(106,2,45);						 //时
	}

/****************告警解除事件记录***********************/
void Dis_Link(void)
	{
	drawASCII(0,0,10);					
	drawword(8,0,36);					//用
	drawASCII(24,0,10);
	drawword(26,0,12);					//电
	drawASCII(42,0,10);						   
	drawword(44,0,55);					//查
	drawASCII(60,0,10);						   
	drawword(62,0,56);					//询
	drawASCII(78,0,10);						   
	drawword(80,0,57);					//终
	drawASCII(96,0,10);						   
	drawword(98,0,58);					//端
	drawword(114,0,40);
 	drawword(0,2,40);					//" "
	drawword(16,2,40);					//" "
	drawASCII(26,2,48);					//L
	drawASCII(34,2,19);					//i
	drawASCII(42,2,24);					//n
	drawASCII(50,2,21);					//k
	drawASCII(58,2,10);					//" "
	drawASCII(66,2,56);					//T
	drawASCII(74,2,15);					//e
	drawASCII(82,2,29);					//s
	drawASCII(90,2,30);					//t
	drawASCII(98,2,63);					//：
	drawASCII(106,2,51);				//O
	drawASCII(114,2,47);				//K


	}
/****************告警解除by按键事件记录***********************/
void Alarm_reg(void)
	{
	uchar i;
	uchar temp[7];
	alarm_canc = 1;
	for(i=0;i<7;i++)
		{
		temp[i] = Byte_Read(LOCAL_REG+i);
		}
	temp[0] = ALAEM_CAC;
	if(temp[1]>0x11)
		{
		temp[1]=0x00;
		}
	temp[1]++;
	temp[2] = Hex2Bcd(Now_time.min);
	temp[3] = Hex2Bcd(Now_time.hour);
	temp[4] = Hex2Bcd(Now_time.day);
	temp[5] = Hex2Bcd(Now_time.mon);
	temp[6] = Hex2Bcd(Now_time.year);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_REG);
	WDT_CONTR = FEED_DOG;
	for(i=0;i<7;i++)
		{
		Byte_Program(LOCAL_REG+i,temp[i]);
		}
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_REG+temp[1]*SECTOR_NO);
	WDT_CONTR = FEED_DOG;
		for(i=0;i<7;i++)
		{
		Byte_Program(LOCAL_REG+temp[1]*SECTOR_NO+i,temp[i]);
		}
	}

/****************告警解除by充值***********************/
void Alarm_Abo(void)
	{
	uchar i;
	uchar temp[7];
	for(i=0;i<7;i++)
		{
		temp[i] = Byte_Read(LOCAL_REG+i);
		}
	temp[0] = ALAEM_CACJ;
	temp[2] = Hex2Bcd(Now_time.min);
	temp[3] = Hex2Bcd(Now_time.hour);
	temp[4] = Hex2Bcd(Now_time.day);
	temp[5] = Hex2Bcd(Now_time.mon);
	temp[6] = Hex2Bcd(Now_time.year);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_REG);
	WDT_CONTR = FEED_DOG;
	for(i=0;i<7;i++)
		{
		Byte_Program(LOCAL_REG+i,temp[i]);
		}
	}
