#include"config.h"


/*************数据头尾及校验*****************/
void CheckHE(void)
	{
	uchar i;
	buffer_flag = 0;
	point = 0;
	data_start = 0;
	if((buffer[0]==HEAD)&&(buffer[7]==HEAD))
		{
		if(buffer[Data_len-1]==END)
			{
			for(i=0;i<Data_len;i++)
				{
				Data_tmp[i]=buffer[i];
				}
			data_t= 1;
			}
		else
			{
			data_t = 0;
			}
		}
	else
		{
		data_t = 0;
		}		
	}

/*************数据求和校验*****************/
void CheckSum(void)
	{
	uchar i,sum;
	CheckHE();
	if(data_t)
		{
		sum = 0;
		for(i=0;i<Data_len-2;i++)
			{
			sum += Data_tmp[i];
			}
		if(Data_tmp[Data_len-2] == sum)
			{
			data_t = 1;
			}
		else
			{
			data_t = 0;
			}
		}
	}

/*************地址校验*****************/
void CheckADD(void)
	{

	uchar mac_add[6];
	uchar i;
	uchar tmp1=0,tmp2=0;
	for(i=0;i<6;i++)
		{
		mac_add[i]=	Byte_Read(LOCAL_MAC+i);
		}
	CheckSum();	
	if(data_t)
		{
		for(i=0;i<6;i++)
			{
			if(Data_tmp[i+1]==0xff)			  		//判断是否广播地址
				{
				tmp1++;
				}
		   	if(Data_tmp[i+1]==mac_add[i])			//判断是否是本机物理地址
		   		{
				tmp2++;
				}
			}
		if(tmp2 == 0x06)
			{
			mac_t = 1;
			data_t = 1;
			}
		else if(tmp1 == 0x06)
			{
			mac_t = 0;
			data_t = 1;
			}
		else 
			{
			data_t = 0;
			}
		}
	}




/*************数据包解析*****************/
void BagPack(void)
	{
	uchar temp=0;
	uchar i;
	CheckADD();
	if(data_t)
		{
		temp = (Data_tmp[8]&0x1f);
		switch(temp)
			{
			case(CON1):
				WDT_CONTR = FEED_DOG;
				if(mac_t)
					{
					Reset_fun();				
					}
				break;
			case(CON2):
				WDT_CONTR = FEED_DOG;
				if(mac_t)
					{
					Set_fun();									
					if(reset)
						{
						timing_flag = 0;
						set_dj = 0;
						set_nowdl = 0;
						set_leadl = 0;
						set_leaje = 0;
						set_timegd = 0;
						set_lastgd = 0;
						set_alldl = 0;
						set_alljs = 0;
						set_lgdt = 0;
						set_ljgdje = 0;
						set_dqjtdj = 0;
						nece1 = 0;
						}
					else
						{
						Report_T();
						}
					}
				break;
			case(CON3):
				WDT_CONTR = FEED_DOG;
				Check_fun();
				break;
			case(CON4):
				WDT_CONTR = FEED_DOG;
				if(mac_t)
					{
					Dbdata_fun();
					Report_T();
					}				
				break;
			case(CON5):
				WDT_CONTR = FEED_DOG;
				Linktest_fun();
				break;
			case(CON6):
				WDT_CONTR = FEED_DOG;
				if(mac_t)
					{
					CheckT_fun();
					}				
				break;
			default:
			    buffer_flag=0;
				break;
			}		
		for(i=0;i<COUNT;i++)
			{
			Data_tmp[i]=0x00;
			}
		}	
	}

/*************复位函数*****************/
void Reset_fun(void)
	{
	uchar Data_Len,Data_Lent;
	Data_Len = 0x0a;
	Data_Lent = Data_tmp[9];
	while(Data_Lent!=0)
		{
		if((Data_tmp[Data_Len] == 0x34))						 //硬件初始化
			{
			Report_T();
			IAP_CONTR = 0x20;				  //软件复位，从0x0000开始重新执行程序
			}
		if((Data_tmp[Data_Len] == 0x35))						 //参数初始化
			{
			Report_T();
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_SETREG);
			IAP_CONTR = 0x20;				  //软件复位，从0x0000开始重新执行程序
			}	
		if((Data_tmp[Data_Len] == 0x36))						 //数据区初始化
			{
			Report_T();
			WDT_CONTR = FEED_DOG;
			Init();
			IAP_CONTR = 0x20;				  //软件复位，从0x0000开始重新执行程序
			}
		if((Data_tmp[Data_Len] == 0x37))					 	//恢复出厂设置
			{
			Report_T();			
			WDT_CONTR = FEED_DOG;
			Init();
			IAP_CONTR = 0x20;				  //软件复位，从0x0000开始重新执行程序	
			}
		else
			{
			WDT_CONTR = FEED_DOG;
			Data_Lent =	0;
			}
		}
	}
	
/*************参数设置*****************/
void Set_fun(void)
	{
	uchar Data_Len,Data_Lent;
	uchar i,temp;
	Data_Len = 0x0a;
	Data_Lent = Data_tmp[9];
	for(i=0;i<Data_Lent;i++)
		{
		Data_tmp[Data_Len+i] -= OFFSET;
		if(((Data_tmp[Data_Len+i]>>4)<=0x09)&&((Data_tmp[Data_Len+i]&0x0f)<=0x09))
			{
			Data_tmp[Data_Len+i] += OFFSET;
			}
		else
			{
			Data_Lent = 0;
			reset = 1;
			}
		}
	while(Data_Lent!=0)
		{
		if((Data_tmp[Data_Len] == 0x34)&&(Data_Lent!=0))						   //本机MAC地址
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_MAC);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<6;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_MAC+i,Data_tmp[Data_Len+1+i]);	   //存放地址
				}
			Data_Len += 0x07;
			Data_Lent -=0x07;					
			}
		if((Data_tmp[Data_Len] == 0x35)&&(Data_Lent!=0))							  //电表地址
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_ADD);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<6;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_ADD+i,Data_tmp[Data_Len+1+i]);	   //存放地址
				}
			Data_Len += 0x07;
			Data_Lent -=0x07;
			}
		if((Data_tmp[Data_Len] == 0x36)&&(Data_Lent!=0))
			{														  //存放密码，此版暂时不用
			Data_Len += 0x03;
			Data_Lent -=0x03;
			}
		if((Data_tmp[Data_Len] == 0x37)&&(Data_Lent!=0))							  //设置时间
			{
			if(mac_t)
				{
				WDT_CONTR = FEED_DOG;
				Sector_Erase(LOCAL_TIMING);
				WDT_CONTR = FEED_DOG;
				for(i=0;i<6;i++)
					{
					Data_tmp[Data_Len+1+i] -= OFFSET;
					Byte_Program(LOCAL_TIMING+i,Data_tmp[Data_Len+1+i]);	   //存放地址
					}
				temp = Data_tmp[Data_Len+1];
				Now_time.sec = Bcd2Hex(temp);
				if(Now_time.sec>=60)
					{
					reset = 1;
					}
				temp = Data_tmp[Data_Len+2];			
				Now_time.min = Bcd2Hex(temp);
				if(Now_time.min>=60)
					{
					reset = 1;
					}
				temp = Data_tmp[Data_Len+3];
				Now_time.hour = Bcd2Hex(temp);
				if(Now_time.hour>=24)
					{
					reset = 1;
					}
				temp = Data_tmp[Data_Len+4];
				Now_time.day = Bcd2Hex(temp);
				if(Now_time.day>=32)
					{
					reset = 1;
					}
				temp = Data_tmp[Data_Len+5];
				Now_time.mon = Bcd2Hex(temp);
				if(Now_time.mon >= 13)
					{
					reset = 1;
					}
				temp = Data_tmp[Data_Len+6];
				Now_time.year = Bcd2Hex(temp); 
				timing_flag = 1;
				set_dj = 0;
				set_nowdl = 0;
				set_leadl = 0;
				set_leaje = 0;
				set_timegd = 0;
				set_lastgd = 0;
				set_alldl = 0;
				set_alljs = 0;
				set_lgdt = 0;
				set_ljgdje = 0;
				set_dqjtdj = 0;
				nece1 = 1;
				timeout = 0;
				Time_Counter = 0;
				}
			Data_Len += 0x07;
			Data_Lent -=0x07;
			} 
		if((Data_tmp[Data_Len] == 0x38)&&(Data_Lent!=0))
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_DALARMVALUE);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_DALARMVALUE+i,Data_tmp[Data_Len+1+i]);	   //本机电量告警值
				}
			Data_Len += 0x05;
			Data_Lent -=0x05;
			}
		if((Data_tmp[Data_Len] == 0x39)&&(Data_Lent!=0))
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_JALARMVALUE);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_JALARMVALUE+i,Data_tmp[Data_Len+1+i]);	   //本机金额告警值
				}
			Data_Len += 0x05;
			Data_Lent -=0x05;
			}
		else
			{
			if((Data_tmp[Data_Len] <= 0x33)||(Data_tmp[Data_Len] >= 0x3a))
				{
				Data_Lent =0;
				}
			}
		}
	}
	
/*************参数查询*****************/
void Check_fun(void)
	{
	uchar Data_Len,Data_Lent;
	uchar i;
	uchar tmp[34],len=0;
	Data_Len = 0x0a;
	Data_Lent = Data_tmp[9];
	while(Data_Lent!=0)
		{
		if((Data_tmp[Data_Len] == 0x34)&&(Data_Lent!=0))						  //查询电表地址
			{
			tmp[len] = 0x34;
			len++;
			WDT_CONTR = FEED_DOG;
			for(i=0;i<6;i++)
				{
				tmp[len] = Byte_Read(LOCAL_MAC+i);	   			  //存放地址 
				tmp[len] += OFFSET;				
				len++;
				}
			Data_Len += 0x01;
			Data_Lent -=0x01;
			}
		if((Data_tmp[Data_Len] == 0x35)&&(Data_Lent!=0))						  //查询MAC地址
			{
			tmp[len] = 0x35;
			len++;
			WDT_CONTR = FEED_DOG;
			for(i=0;i<6;i++)
				{
				tmp[len] = Byte_Read(LOCAL_ADD+i);	   			  //存放地址 
				tmp[len] += OFFSET;				
				len++;
				}
			Data_Len += 0x01;
			Data_Lent -=0x01;
			}
		if((Data_tmp[Data_Len] == 0x36)&&(Data_Lent!=0))
			{														  //查询密码，此版暂时不用
			tmp[len] = 0x36;
			len++;
			tmp[len] = VERL;										  //版本号低位
			len++;
			tmp[len] = VERH;										  //版本号高位
			len++;
			Data_Len += 0x01;
			Data_Lent -=0x01;
			}
		if((Data_tmp[Data_Len] == 0x37)&&(Data_Lent!=0))							  //查询时间
			{
			tmp[len] = 0x37;
			len++;
			WDT_CONTR = FEED_DOG;
			tmp[len] = Hex2Bcd(Now_time.sec);
			tmp[len] += OFFSET;
			len++;
			tmp[len] = Hex2Bcd(Now_time.min);
			tmp[len] += OFFSET;
			len++;
			tmp[len] = Hex2Bcd(Now_time.hour);
			tmp[len] += OFFSET;
			len++;
			tmp[len] = Hex2Bcd(Now_time.day);
			tmp[len] += OFFSET;
			len++;
			tmp[len] = Hex2Bcd(Now_time.mon);
			tmp[len] += OFFSET;
			len++;
			tmp[len] = Hex2Bcd(Now_time.year);
			tmp[len] += OFFSET;
			len++;								
			Data_Len += 0x01;
			Data_Lent -=0x01;
			} 
		if((Data_tmp[Data_Len] == 0x38)&&(Data_Lent!=0))
			{
			tmp[len] = 0x38;
			len++;
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{				
				tmp[len] = Byte_Read(LOCAL_DALARMVALUE+i);	   //查询本机电量告警值
				tmp[len] += OFFSET;
				len++;
				}
			Data_Len += 0x01;
			Data_Lent -=0x01;
			}
		if((Data_tmp[Data_Len] == 0x39)&&(Data_Lent!=0))
			{
			tmp[len] = 0x39;
			len++;
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{				
				tmp[len] = Byte_Read(LOCAL_JALARMVALUE+i);	   //查询本机金额告警值
				tmp[len] += OFFSET;
				len++;
				}
			Data_Len += 0x01;
			Data_Lent -=0x01;
			}
		else
			{
			if((Data_tmp[Data_Len] <= 0x33)||(Data_tmp[Data_Len] >= 0x3a))
				{
				Data_Lent =0;
				}
			}
		}
	WDT_CONTR = FEED_DOG;
	Data_tmp[0]=HEAD;
	for(i=0;i<6;i++)
		{
		Data_tmp[1+i]=Byte_Read(LOCAL_MAC+i);
		}
	Data_tmp[7]=HEAD;
	Data_tmp[8]=0x83;
	Data_tmp[9]=len;
	for(i=0;i<len;i++)
		{
		Data_tmp[10+i] = tmp[i];
		}
	Data_tmp[10+len] = 0x00;
	for(i=0;i<(10+len);i++)
		{
		Data_tmp[10+len] += Data_tmp[i];
		}
	Data_tmp[11+len] = END;
	WDT_CONTR = FEED_DOG;
	COM_send(12+len);  
	}
	
/*************电表数据*****************/
void Dbdata_fun(void)
	{
	uchar Data_Len,Data_Lent;
	uchar i;
	Data_Len = 0x0a;
	Data_Lent = Data_tmp[9];
	while(Data_Lent!=0)
		{
		if((Data_tmp[Data_Len] == D_FL)&&(Data_Lent!=0))								  //电表费率
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_ELEVALUE);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<16;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_ELEVALUE+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}	   
				}
			if((i == 16)&&(nece1||nece2))
				{
				set_dj = 1;
				Byte_Program(LOCAL_SETREG,0x55);							 //已设置电价				
				}
			else
				{
				set_dj = 0;
				}
			Data_Len += 0x11;
			Data_Lent -=0x11;
			}

		if((Data_tmp[Data_Len] == D_ZT)&&(Data_Lent!=0))								  //电表状态
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_ELESTA);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<14;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_ELESTA+i,Data_tmp[Data_Len+1+i]);	   
				}
			Data_Len += 15;
			Data_Lent -=15;
			Alarm_jude();
			}

		if((Data_tmp[Data_Len] == D_N_M)&&(Data_Lent!=0))								  //当前电表表码
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_NELEVAL);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<20;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_NELEVAL+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 20)&&(nece1||nece2))
				{
				set_nowdl = 1;
				Byte_Program(LOCAL_SETREG+1,0x55);							  //已设置电量					
				}
			else
				{
				set_nowdl = 0;
				}
			Data_Len += 21;
			Data_Lent -=21;
			}

		if((Data_tmp[Data_Len] == D_L_D)&&(Data_Lent!=0))								   //当前剩余电量
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_NELELEAD);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<5;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_NELELEAD+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 5)&&(nece1||nece2))
				{
				set_leadl = 1;
				Byte_Program(LOCAL_SETREG+2,0x55);							  //已设置剩余电量 				
				}
			else
				{
				set_leadl = 0;
				}
			Data_Len += 6;
			Data_Lent -=6;
			}

		if((Data_tmp[Data_Len] == D_L_J)&&(Data_Lent!=0))								   //当前剩余金额
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_NELELEAJ);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<5;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_NELELEAJ+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 5)&&(nece1||nece2))
				{
				set_leaje = 1;
				Byte_Program(LOCAL_SETREG+3,0x55);						   //已设置剩余金额
				}
			else
				{
				set_leaje = 0;
				}
			Data_Len += 6;
			Data_Lent -=6;															 
			}

		if((Data_tmp[Data_Len] == D_T_G)&&(Data_Lent!=0))								  //总购电次数
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_BUYCONUT);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<2;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_BUYCONUT+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 2)&&(nece1||nece2))
				{
				set_timegd = 1;
				Byte_Program(LOCAL_SETREG+4,0x55);							//已设置总购电次数					
				}
			else
				{
				set_timegd = 0;	
				}
			Data_Len += 3;
			Data_Lent -=3;
			}

		if((Data_tmp[Data_Len] == D_LAST_G)&&(Data_Lent!=0))							  //最后一次够电时间
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_LASTBUYT);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<5;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_LASTBUYT+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 5)&&(nece1||nece2)&&(Data_tmp[Data_Len+3]!=0)&&(Data_tmp[Data_Len+4]!=0))
				{
				set_lastgd = 1;
				Byte_Program(LOCAL_SETREG+5,0x55);							//已设置最后一次购电时间				
				}
			else
				{
				set_lastgd = 0;
				}
			Data_Len += 6;
			Data_Lent -= 6;
			}

		if((Data_tmp[Data_Len] == D_N_TAT)&&(Data_Lent!=0))									//当前结算累计
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_ALLDL);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<20;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_ALLDL+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 20)&&(nece1||nece2))
				{
				set_alljs = 1;
				Byte_Program(LOCAL_SETREG+6,0x55);								//已设置当前结算累计
				}
			else
				{
				set_alljs = 0;
				}
			Data_Len += 21;
			Data_Lent -=21;
			}

		if((Data_tmp[Data_Len] == D_L_TAT)&&(Data_Lent!=0))									//上一结算累计
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_LASTALLDL);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				Byte_Program(LOCAL_LASTALLDL+i,Data_tmp[Data_Len+1+i]);	  
				}
//			set_l1js = 1;
			Byte_Program(LOCAL_SETREG+7,0x55);
			Data_Len += 5;
			Data_Lent -=5;
			}
		if((Data_tmp[Data_Len] == D_G_TAT)&&(Data_Lent!=0))								  //累计购电量
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_BUYTAT);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_BUYTAT+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 4)&&(nece1||nece2))
				{
				set_alldl = 1;
				Byte_Program(LOCAL_SETREG+8,0x55);							   //已设置累计购电量
				}
			else
				{
				set_alldl = 0;
				}
			Data_Len += 5;
			Data_Lent -=5;
			}

		if((Data_tmp[Data_Len] == D_G_LTJE)&&(Data_Lent!=0))							  //最后一次购电金额
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_LASTGDJE);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_LASTGDJE+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 4)&&(nece1||nece2))
				{
				set_lgdt = 1;
				Byte_Program(LOCAL_SETREG+9,0x55);							   //已设置最后一次购电金额
				}
			else
				{
				set_lgdt = 0;
				}
			Data_Len += 5;
			Data_Lent -=5;
			}

		if((Data_tmp[Data_Len] == D_L_JINE)&&(Data_Lent!=0))							  //累计购电金额
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_LJGDJNE);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_LJGDJNE+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 4)&&(nece1||nece2))
				{
				set_ljgdje = 1;
				Byte_Program(LOCAL_SETREG+10,0x55);							   //已设置累计购电金额
				}
			else
				{
				set_ljgdje = 0;
				}
			Data_Len += 5;
			Data_Lent -=5;
			}	
				
		if((Data_tmp[Data_Len] == D_JTDJ)&&(Data_Lent!=0))							  //当前阶梯电价
			{
			WDT_CONTR = FEED_DOG;
			Sector_Erase(LOCAL_DQJTDJ);
			WDT_CONTR = FEED_DOG;
			for(i=0;i<4;i++)
				{
				Data_tmp[Data_Len+1+i] -= OFFSET;
				if(((Data_tmp[Data_Len+1+i]>>4)<=0x09)&&((Data_tmp[Data_Len+1+i]&0x0f)<=0x09))
					{
					Byte_Program(LOCAL_DQJTDJ+i,Data_tmp[Data_Len+1+i]);
					}
				else
					{
					break;
					}
				}
			if((i == 4)&&(nece1||nece2))
				{
				set_dqjtdj = 1;
				Byte_Program(LOCAL_SETREG+11,0x55);							   //已设置阶梯电价
				}
			else
				{
				set_dqjtdj = 0;
				}
			Data_Len += 5;
			Data_Lent -=5;
			}		
		else
			{
			if((Data_tmp[Data_Len] <= 0x33)||(Data_tmp[Data_Len] >= D_JTDJ+1))
				{
				Data_Lent =0;
				}
			}
		}
	}
/*************链路测试*****************/
void Linktest_fun(void)
	{
	linktest = 1;
	lcd_flag = 1;
	}
	
		
/*************事件查询*****************/
void CheckT_fun(void)
	{
	uchar i,k;
	uchar temp[7];
   	WDT_CONTR = FEED_DOG;
	i = Data_tmp[10]-OFFSET;
	if(0x00 == i)
		{
		for(k=0;k<7;k++)
			{
			temp[k] = Byte_Read(LOCAL_REG+k);
			}
		}
	else
		{
		k=Byte_Read(LOCAL_REG+1);
		if(k >= i)
			{
			i = k-i+1;
			}
		else
			{
			i = 17+k-i;
			}			
		for(k=0;k<7;k++)
			{
			temp[k] = Byte_Read(LOCAL_REG+SECTOR_NO*i+k);
			}
	    }
		WDT_CONTR = FEED_DOG;
		if((temp[0]!=ALAEM_CAC)&&(temp[0]!=ALAEM_CACJ))
			{
			k=0;
			}
		else
			{
			k=7;
			}		
	Data_tmp[0]=HEAD;
	for(i=0;i<6;i++)
		{
		Data_tmp[1+i]=Byte_Read(LOCAL_MAC+i);
		}
	Data_tmp[7]=HEAD;
	Data_tmp[8]=0x86;
	Data_tmp[9]=0x01+k;
	//Data_tmp[10]=0x34;
	for(i=0;i<k;i++)
		{
		Data_tmp[11+i] = temp[i]+0x33;
		}
	Data_tmp[11+k] = 0x00;
	for(i=0;i<(11+k);i++)
		{
		Data_tmp[11+k] += Data_tmp[i];
		}
	Data_tmp[12+k] = END;
	WDT_CONTR = FEED_DOG;
	COM_send(13+k);
	}	
	
/*************确认应答*****************/
void Report_T(void)
	{
	uchar i;
	Data_tmp[0]=0xa8;
		for(i=0;i<6;i++)
		{
		Data_tmp[1+i]=Byte_Read(LOCAL_MAC+i);
		}
	Data_tmp[7]=0xa8;
	Data_tmp[8]=0x80;
	Data_tmp[9]=0x01;
	Data_tmp[10]=0x34;				   //确认
	Data_tmp[11]=0x00;
	for(i=0;i<11;i++)
		{
		Data_tmp[11] += Data_tmp[i];
		}
	Data_tmp[12]=0x16;
	WDT_CONTR = FEED_DOG;
	COM_send(13);
	}
/*************否认应答*****************/ 	
//void Report_F(void)
//	{
//	uchar i;
//	Data_tmp[0]=0xa8;
//		for(i=0;i<6;i++)
//		{
//		Data_tmp[1+i]=Byte_Read(LOCAL_ADD+i);
//		}
//	Data_tmp[7]=0xa8;
//	Data_tmp[8]=0x80;
//	Data_tmp[9]=0x01;
//	Data_tmp[10]=0x02;
//	Data_tmp[11]=0x00;
//	for(i=0;i<11;i++)
//		{
//		Data_tmp[11] += Data_tmp[i];
//		}
//	Data_tmp[12]=0x16;
//	COM_send(13);
//	}	
	
		
/*************预跳闸判断*****************/
void Alarm_jude(void)
	{
	uchar i;
	i = Byte_Read(LOCAL_ELESTA+4);
	if((i&0x80) == 0x80)
		{
		alarm_flg = 1;
		Alarm_count = 0;
		alarm_time0 = 1;
		}
	else
		{
		alarm_flg = 0;
		alarm_canc = 0;
		ALARM = 1;
		A_LED = 0;
		Alarm_Abo();
		}
	}	

/*************初始化所有数据扇区*****************/
void Init(void)
	{
	uchar i;
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_ADD);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_PASS);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_DALARMVALUE);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_JALARMVALUE);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_ELEVALUE);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_ELESTA);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_NELEVAL);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_NELELEAD);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_NELELEAJ);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_BUYCONUT);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_LASTBUYT);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_ALLDL);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_LASTALLDL);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_BUYTAT);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_SETREG);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_LASTGDJE);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_LJGDJNE);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_DQJTDJ);
	WDT_CONTR = FEED_DOG;
	Sector_Erase(LOCAL_TIMING);
	for(i=0;i<0x11;i++)
		{
		WDT_CONTR = FEED_DOG;
		Sector_Erase(LOCAL_REG+i*SECTOR_NO);
		}
	}

/*************HEX转BCD*****************/
uchar Hex2Bcd(uchar b)
{
	uchar bcd ;

	bcd = ((b/10)<<4) | (b%10) ;

	return bcd ;
}

/*************BCD转HEX*****************/
uchar Bcd2Hex(uchar b)
{
	uchar hex ;

	hex = (((b&0xf0)>>4)*10) + (b&0x0f) ;

	return hex ;
}

									 