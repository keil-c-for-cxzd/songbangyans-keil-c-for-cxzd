#include"config.h"

/*************串口初始化*****************/

void UART_init()		   	//初始化串行口和波特率发生器 
	{	                 
	SCON = 0xd0;           	//选择串口工作方式3，打开接收允许
	BRT = RELOAD;		 	//使用独立的波特率发生器
	AUXR = 0x11;		 	//启用独立波特率计数器
	AUXR1 = 0x80;		 	//串口从P3切换到P1
	point = 0;
	ES=1;                	//允许串行口中断
	PS=1;                	//设计串行口中断优先级
	EA =1;               	//单片机中断允许
	}
/*************串口中断处理函数*****************/

com_interrupt(void) interrupt 4 using 3
	{
	if(RI)                                //处理接收中断
		{
		uchar temp;
		temp = SBUF;
		RI=0;							  //清除中断标志位
		COM_LED = 1;					  //通信指示灯亮			 
//		for(i=0;i<8;i++)
//			{
//			if((temp>>i&0x01) == 0x01)
//				{
//				j++;
//				}
//			}
//		if(((j%2) == 0)&&(RB8 == 0)) 
//			{
//			if((temp == 0xa8)||data_start)
//				{
//				data_start = 1;
//				buffer[point] = temp;
//				point++;
//				}
//			}
//		else if(((j%2) != 0)&&(RB8 == 1))
//			{
//			if((temp == 0xa8)||data_start)
//				{
//				data_start = 1;
//				buffer[point] = temp;
//				point++;
//				}
//			}
		ACC = temp;
		if(RB8&&P)
			{
			if((temp == 0xa8)||data_start)
				{
				data_start = 1;
				buffer[point] = temp;
				point++;
				}			
			}
		else if((!RB8)&&(!P))
			{
			if((temp == 0xa8)||data_start)
				{
				data_start = 1;
				buffer[point] = temp;
				point++;
				}			
			}			                             
		if(point == 0x0a)
			{
			Data_len = buffer[9]+12;			
			}
		if(point == Data_len)
			{
			buffer_flag=1;
			}
		Data_time = 0;
		COM_LED = 0;					  //通信指示灯灭		
		}
	if(TI)                               //处理发送中断
		{
		TI=0;
		}
	}
/*************串口发送函数*****************/

void COM_send(uchar nCount)		 //采用查询发送方式发送
	{
	uchar temp;
	uchar k;
	COM_LED = 1;                 //通信指示灯亮  
	for(k=0;k<nCount;k++)	                                           
		{
		temp = Data_tmp[k];
//		for(i=0;i<8;i++)
//			{
//			if((temp>>i&0x01) == 0x01)
//				{
//				j++;
//				}
//			}
//		if((j%2) == 0) 
//			{
//			TB8 = 0;
//			}
//		else 
//			{
//			TB8 = 1;
//			}
		ACC = temp;
		TB8 = P;
		SBUF=temp;			 //把缓存区的数据都发送到串口
		while(TI == 0);
		TI=0;
		}
	COM_LED = 0;             //通信指示灯灭      
	}

//增加测试功能
